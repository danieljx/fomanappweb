/* jshint node: true */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'fomanappweb',
    environment: environment,
    baseURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }/*,
    contentSecurityPolicy: {
      'default-src': "'self' http://dev.nelibox.com ws://dev.nelibox.com:49152 http://dev.nelibox.com/ferney/cfserver",
      'script-src': "'self' unsafe-eval ws://dev.nelibox.com:49152 ws://dev.nelibox.com:49153 http://dev.nelibox.com:49153 http://dev.nelibox.com:49152 http://dev.nelibox.com:35729 http://dev.nelibox.com:4200", // Allow scripts from https://cdn.mxpnl.com
      'font-src': "'self' data: chrome-extension-resource: https://fonts.gstatic.com https://fonts.googleapis.com http://fonts.gstatic.com  http://dev.nelibox.com:4200 ws://dev.nelibox.com:35729", // Allow fonts to be loaded from http://fonts.gstatic.com
      'connect-src': " 'self'  http://dev.nelibox.com http://dev.nelibox.com:4200 ws://dev.nelibox.com:49153 ws://dev.nelibox.com:49152/livereload http://dev.nelibox.com/ferney/cfserver http://dev.nelibox.com:35729 ws://dev.nelibox.com:35729 ws://dev.nelibox.com:35729/livereload", // Allow data (ajax/websocket) from api.mixpanel.com and custom-api.local
      'img-src': "'self' data: http://dev.nelibox.com data: https://www.google.com",
      'style-src': "'self' 'unsafe-inline' data: chrome-extension-resource:  fonts.gstatic.com fonts.googleapis.com", // Allow inline styles and loaded CSS from http://fonts.googleapis.com 
      'media-src': "'self' data: chrome-extension-resource:",
      'frame-src': "'self' data: chrome-extension-resource: http://dev.nelibox.com"
    }*/
  };
  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
    ENV.apiHost= 'http://localhost:3000/api/';
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.baseURL = '/';
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {

  }

  return ENV;
};
