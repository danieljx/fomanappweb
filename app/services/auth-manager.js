import Ember from 'ember';

export default Ember.Service.extend({
  accessToken: null,
  accessUser: null,
  tempTransition: null,
  authenticate(user, userPass) {
    return Ember.$.ajax({
      method: "POST",
      url: "http://localhost:3000/api/authenticate",
      data: {
        user: user, 
        userPass: userPass 
      }
    }).then((result) => {
      if(result.success) {
        result.userId   = result.user._id;
        result.userName = result.user.userName;
        this.setAuthenticate(result);
      }
    });
  },
  setAuthenticate(result) {
    this.set('accessToken', result.token);
    this.set('accessUserId', result.userId);
    this.set('accessUserName', result.userName);
    this.set('isAuthenticated', result.success);

    Ember.$.cookie('accessToken', result.token);
    Ember.$.cookie('accessUserId', result.userId);
    Ember.$.cookie('accessUserName', result.userName);
    Ember.$.cookie('isAuthenticated', result.success);
  },
  invalidate() {
    this.set('accessToken', null);
    this.set('accessUserId', null);
    this.set('accessUserName', null);
    this.set('isAuthenticated', false);
    Ember.$.removeCookie('accessToken');
    Ember.$.removeCookie('accessUserId');
    Ember.$.removeCookie('accessUserName');
    Ember.$.removeCookie('isAuthenticated');
  },
  isAuthenticated: false
});