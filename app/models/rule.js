import DS from 'ember-data';

export default DS.Model.extend({
    ruleName: DS.attr('string'),
    ruleSta: DS.attr('boolean'),
  	dateIn: DS.attr('date'),
  	user: DS.belongsTo('user')
});