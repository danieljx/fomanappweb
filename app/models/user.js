import DS from 'ember-data';

export default DS.Model.extend({
    userName: DS.attr('string'),
    user: DS.attr('string'),
    userPass: DS.attr('string'),
    userSta: DS.attr('boolean'),
    userRule: DS.hasMany('rule'),
  	dateIn: DS.attr('date'),
  	functionary: DS.belongsTo('functionary'),
  	training: DS.belongsTo('training')
});