import DS from 'ember-data';

export default DS.Model.extend({
    trainName: DS.attr('string'),
    trainHouer: DS.attr('number'),
    trainSta: DS.attr('boolean'),
  	dateIn: DS.attr('date'),
    userIn: DS.hasMany('user'),
  	functionary: DS.belongsTo('functionary')
});