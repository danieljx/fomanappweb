import DS from 'ember-data';

export default DS.Model.extend({
    funcName: DS.attr('string'),
    funcEmail: DS.attr('string'),
    funcPhone: DS.attr('string'),
    funcNit: DS.attr('number'),
    funcSta: DS.attr('boolean'),
  	funcCompany: DS.attr('string'),
    //funcTrain: DS.hasMany('training'),
  	//funcUser: DS.hasMany('user'),
  	funcIn: DS.attr('date'),
  	funcEnd: DS.attr('date'),
  	dateIn: DS.attr('date'),
  	//userIn: DS.hasMany('user')
});