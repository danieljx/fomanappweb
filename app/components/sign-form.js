import Ember from 'ember';
export default Ember.Component.extend({
	actions: {
		authenticate() {
			var dataCredet = this.getProperties('user', 'userPass');
			this.get('session').authenticate(dataCredet.user, dataCredet.userPass).then(() => {
				var transition = this.get('session').get('tempTransition');
					if(transition) {
						transition.retry();
					} else {
						this.get('router').transitionTo('dashboard');
					}
			}, (err) => {
				console.log('Error obtaining token: ' + err.responseText);
			});
		}
	}
});