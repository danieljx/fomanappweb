import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
	this.route('search', { path: '/:funcNit' });
	this.route('login', function() {
		this.route('sign');
		this.route('signup');
		this.route('reset');
		this.route('password', {});
	});
	this.route('dashboard');
	this.route('account', function() {
		this.route('list', { path: '/' });
		this.route('new');
		this.route('edit', { path: '/:userId' });
	});
	this.route('functionary', function() {
		this.route('list', { path: '/' });
		this.route('edit', { path: '/:functId' });
	});
	this.route('train', function() {
		this.route('list', { path: '/' });
		this.route('edit', { path: '/:trainId' });
	});
	this.route('company', function() {
		this.route('list', { path: '/' });
		this.route('edit', { path: '/:companyId' });
	});
	this.route('logout');
});

export default Router;
