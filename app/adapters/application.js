import Ember from 'ember';
import DS from "ember-data";
var ApplicationAdapter = DS.RESTAdapter.extend({
    host: 'http://localhost:3000',
    namespace: 'api',
	authManager: Ember.inject.service(),
	headers: Ember.computed('authManager.accessToken', function() {
		return {
			"Authorization": `${this.get("authManager.accessToken")}`
		};
	}),
	shouldReloadRecord: function(store) {
		return false;
	},
	shouldReloadAll: function(store) {
		return false;
	},
	shouldBackgroundReloadRecord: function(store) {
		return true;
	},
	shouldBackgroundReloadAll: function(store) {
		return true;
	}
});
export default ApplicationAdapter;