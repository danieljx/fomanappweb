import Ember from 'ember';
export default Ember.Route.extend({
	beforeModel: function(transition) {
		if(!Ember.$.cookie('isAuthenticated')) {
			console.log("cookie not isAuthenticated");
			console.log(transition);
			this.get('session').set('tempTransition', transition);
			this.transitionTo('login.sign');
		} else {
			var sessCokie = {};
			sessCokie.success	= Ember.$.cookie('isAuthenticated');
			sessCokie.userId	= Ember.$.cookie('accessUserId');
			sessCokie.userName	= Ember.$.cookie('accessUserName');
			sessCokie.token		= Ember.$.cookie('accessToken');
			this.get('session').setAuthenticate(sessCokie);
			this.get('session').set('tempTransition', null);
		}
	}
});