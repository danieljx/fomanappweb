import Ember from 'ember';
export default Ember.Route.extend({
	model: function(param) {
		if(param.funcNit) {
			return this.get('store').queryRecord('functionary',{ filter: {funcNit: param.funcNit}});
		}
	},
	setupController: function(controller, models) {
		this._super(controller, models);
		controller.setProperties(models);
	}
});