import Ember from 'ember';
import mixinRoute from '../mixinRoute';
export default mixinRoute.extend({
	model: function(params) {
		return this.get('store').findRecord('functionary', params.functId);
	}
});