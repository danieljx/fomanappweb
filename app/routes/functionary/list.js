import mixinRoute from '../mixinRoute';
export default mixinRoute.extend({
	model: function() {
		
	},
	actions: {
		new() {
			this.transitionTo('functionary.edit',{funct:""});
		},
		edit(funct) {
			this.transitionTo('functionary.edit',{funct:funct._id});
		}
	}
});