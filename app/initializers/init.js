export function initialize(container, app) {
    app.inject('route', 'session', 'service:auth-manager');
    app.inject('controller', 'session', 'service:auth-manager');
    app.inject('template', 'session', 'service:auth-manager');
    app.inject('component', 'session', 'service:auth-manager');
    app.inject('component', 'router', 'router:main');
}

export default {
   
  name: 'init',
  initialize: initialize
};